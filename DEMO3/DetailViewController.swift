//
//  DetailViewController.swift
//  DEMO3
//
//  Created by Aimé Galmi on 25/02/16.
//  Copyright © 2016 Aimé Galmi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet var image_element: UIImageView!
    @IBOutlet var secondLabel : UILabel!

    var detailItem: MyData? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
             if let label2 = self.detailDescriptionLabel {
                
                label2.text  =  ""
                
          //      label.text += " "
                let label = UILabel(frame: CGRectMake(50, 0, 175, 250))
                //label.center = CGPointMake(160, 284)
                label.numberOfLines = 10
                label.textAlignment = NSTextAlignment.Center
                label.text = detail.forename + " " + detail.name
                if !(detail.mail.isEmpty){
                    label.text = label.text! + "\n" + detail.mail
                }
                if !(detail.desk.isEmpty){
                label.text = label.text! + "\n Bureau " +  detail.desk
                }
                if !(detail.adress.isEmpty){
                    label.text = label.text! + "\n" + detail.adress
                }
                
                if !(detail.phone.isEmpty) && !(detail.phone == "NC"){
                    let labelTap2=UITapGestureRecognizer(target: self, action: "phonelabelTapped:")
                    let label3 = UILabel(frame: CGRectMake(50, 175, 300, 25))
                    label3.textAlignment = NSTextAlignment.Center
                    label3.numberOfLines = 2
                    label3.addGestureRecognizer(labelTap2)
                    let url = try! NSAttributedString(
                        data: "Téléphone: <a href=\"\(detail.phone)\">\(detail.phone)</a>".dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!,
                        options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                        documentAttributes: nil)
                    label3.attributedText = url
                    label3.userInteractionEnabled = true;
                    view.addSubview(label3)
                    
                }
                if !(detail.website.isEmpty){
                    let labelTap1=UITapGestureRecognizer(target: self, action: "urllabelTapped:")
                    let label2 = UILabel(frame: CGRectMake(50, 200, 300, 50))
                    label2.textAlignment = NSTextAlignment.Center
                    label2.numberOfLines = 2
                    label2.addGestureRecognizer(labelTap1)
                    let url = try! NSAttributedString(
                        data: "Site : <a href=\"\(detail.website)\">\(detail.website)</a>".dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!,
                        options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                        documentAttributes: nil)
                    label2.attributedText = url
                    label2.userInteractionEnabled = true;
                    view.addSubview(label2)
                }
                self.view.addSubview(label)
                if !(detail.picture.isEmpty){
                    print("|"+detail.picture+"|")
                   let url = NSURL(string: detail.picture)
                    if let data = NSData(contentsOfURL: url!){ //make sure your image in this url does exist, otherwise unwrap in a if let check
                      
                        let image = UIImage(data: data)
                        let imageView = UIImageView(image: image!)
                        imageView.frame = CGRect(x: 100, y: 250, width: 200, height: 200)
                      
                        view.addSubview(imageView)
                    }
                    
                    
                }

                
                
            }
        }
    }
    
    @IBAction func urllabelTapped(sender:UILabel!) {
        
        if let detail = self.detailItem {
            if let url = NSURL(string: detail.website) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        
    }
    
    @IBAction func phonelabelTapped(sender:UILabel!) {
        
        if let detail = self.detailItem {
            if let url =  NSURL(string:"tel://\(detail.phone)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

