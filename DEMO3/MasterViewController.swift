//
//  MasterViewController.swift
//  DEMO3
//
//  Created by Aimé Galmi on 25/02/16.
//  Copyright © 2016 Aimé Galmi. All rights reserved.
//

import UIKit

extension String
{
    func trim() -> String
    {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
}

class MyData {
    let name:String
    let forename: String
    let mail: String
    let desk:String
    let adress:String
    let phone: String
    let website: String
    let picture: String
    init(name: String,forename: String, mail:String, desk:String, adress:String, phone:String, website:String, picture:String){
        self.name = name
        self.mail = mail
        self.forename = forename
        self.desk = desk
        self.adress = adress
        self.phone = phone
        self.picture = picture
        self.website = website
    }
  var description:String {
        get {return "\(name) \(forename)"}
    }
}

class MasterViewController: UITableViewController , NSXMLParserDelegate{
    @IBOutlet var tbData : UITableView?
     var parser = NSXMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var name = ""
    var forename = ""
    var mail = ""
    var desk = ""
    var adress = ""
    var phone = ""
    var website  = ""
    var picture = ""
    

    var detailViewController: DetailViewController? = nil
    var objects = [MyData]()


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()

        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
               beginParsing()
        objects.sortInPlace({ $0.name < $1.name })

        
    }
    
    func beginParsing()
    {
        posts = []
        parser = NSXMLParser(contentsOfURL:(NSURL(string:"https://www.irif.univ-paris-diderot.fr/~yunes/xml/teachers.xml"))!)!
        parser.delegate = self
        parser.parse()
        
        //tbData!.reloadData()
    }
    
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName
        if (elementName as NSString).isEqualToString("teacher")
        {

            name = ""
            forename = ""
            mail = ""
            desk = ""
            adress = ""
            phone = ""
            website  = ""
            picture = ""
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String)
    {
        if element.isEqualToString("name") {
            name += string.trim()
        } else if element.isEqualToString("forename") {
            forename += string.trim()
        }else if element.isEqualToString("mail") {
            mail += string.trim()
        }else if element.isEqualToString("adress") {
            adress += string.trim()
        }else if element.isEqualToString("desk") {
            desk += string.trim()
        }else if element.isEqualToString("phone") {
            phone += string.trim()
        }else if element.isEqualToString("website") {
            website += string.trim()
        }else if element.isEqualToString("picture") {
            picture += string.trim()
        }
    }
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if (elementName as NSString).isEqualToString("teacher") {
             objects.append(MyData(name:name, forename: forename, mail: mail,desk: desk,adress: adress,phone: phone,website: website,picture: picture))
        }
    }
   

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func insertNewObject(sender: AnyObject) {
        objects.insert(MyData(name: "Inconnu", forename: "Inconnu", mail: mail,desk: desk,adress: adress,phone: phone,website: website,picture: picture), atIndex: 0)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row]
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        let object = objects[indexPath.row]
        cell.textLabel!.text = object.description
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            objects.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }


}

